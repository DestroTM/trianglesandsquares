#include <GL/glut.h>

//gcc main.cpp -o main -lGL -lGLU -lglut -lstdc++

static GLfloat BLUE[3] = {0.0f, 0.0f ,1.0f};
static GLfloat ORANGE[3] = {1.0f, 0.5f, 0.0f};
static GLfloat GREEN[3] = {0.0f, 1.0f, 0.0f};
static GLfloat PINK[3] = {1.0f, 0.0f, 1.0f};
static GLfloat YELLOW[3] = {1.0f, 1.0f, 0.0f};
static GLfloat RED[3] = {1.0, 0.0f, 0.0f};

static GLfloat objectRotation = 0.0f;
static GLfloat triangleRotation = 0.0f;
static GLfloat distanceFromCenter = 0.0f;
static GLfloat motionSpeed = 0.1f;

void rotateTriangle() {
    glTranslatef(0.333333f, 0.333333f, 0.0f);
    glRotatef(triangleRotation, 0.0f, 0.0f, 1.0f);
    glTranslatef(-0.333333f, -0.333333f, 0.0f);
}

void drawTriangle(GLfloat colour[]) {
    glBegin(GL_TRIANGLES);
        glColor3f(colour[0], colour[1], colour[2]);
        glVertex2f(0.0,0.0);
        glVertex2f(1.0,0.0);
        glVertex2f(0.0,1.0);
    glEnd();
}

void borderTriangle() {
    glBegin(GL_LINE_STRIP);
        glColor3f(0.0, 0.0, 0.0);
        glVertex2f(0.0f, 0.0f);
        glVertex2f(1.0f, 0.0f);
        glVertex2f(0.0f, 1.0f);
        glVertex2f(0.0f, 0.0f);
    glEnd();
}

void moveFromCenter(GLfloat rotateAngle) {
    glRotatef(rotateAngle, 0.0f, 0.0f, 1.0f);
    glTranslatef(distanceFromCenter, 0.0f, 0.0f);
    glRotatef(-rotateAngle, 0.0f, 0.0f, 1.0f);
}

void triangle(GLfloat colour[], GLfloat rotateAngle, GLfloat translateX, GLfloat translateY) {
    glPushMatrix();
        glScalef(0.1f, 0.1f, 0.1f);
        moveFromCenter(rotateAngle);
        glTranslatef(translateX, translateY, 0.0f);
        rotateTriangle();
        drawTriangle(colour);
        borderTriangle();
    glPopMatrix();
}

void quarter(int quarterNo) {
    glPushMatrix();
        glRotatef(quarterNo * 90.0f, 0.0f, 0.0f, 1.0f);
        glPushMatrix();
            triangle(BLUE, 45.0f, 0.0f, 0.0f);
            triangle(ORANGE, 14.03624347f, 1.0f, 0.0f);
            triangle(PINK, 8.130102354f, 2.0f, 0.0f);
            triangle(GREEN, 90.0f - 14.03624347f, 0.0f, 1.0f);
            triangle(YELLOW, 45.0f, 1.0f, 1.0f);
            triangle(RED, 90.0f - 8.130102354f, 0.0f, 2.0f);
        glPopMatrix();
    glPopMatrix();
}

void rotateObject() {
    glLoadIdentity();
    glRotatef(objectRotation, 0.0f, 0.0f, 1.0f);

    quarter(0);
    quarter(1);
    quarter(2);
    quarter(3);
}

void display() {
    glClearColor (1.0f, 1.0f, 1.0f, 1.0f);
    glViewport(0, 0, 600, 600);
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    rotateObject();
    glutSwapBuffers();
    glFlush();
}

void motion() {
    objectRotation += (motionSpeed * 1);
    if (objectRotation > 360) {
        objectRotation = 0.0f;
    }

    triangleRotation -= (motionSpeed * 3);
    if (triangleRotation < 0) {
        triangleRotation = 360.0f;
    }

    distanceFromCenter += (motionSpeed * (3.0f / (10 * 360)));

    glutPostRedisplay();
}


int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA);
    glutInitWindowSize(600, 600);
    glutCreateWindow("Rotating Triangles");
    glutDisplayFunc(display);
    glutIdleFunc(motion);
    glutMainLoop();

    return 0;
}